@backend-paris
Feature: Backend APIs Paris

  Background: Precondiciones
    * def req_token = read('../../../request/backend/login_get_token/token.json')
    * def res_token = read('../../../response/backend/login_get_token/token.json')
    * def token_unauthorized = read('../../../response/backend/login_get_token/token_unauthorized.json')
    * def res_get_topics = read('../../../response/backend/get_topics_paris.json')
    * def res_get_bussiness_rules = read('../../../response/backend/get_bussiness_rules.json')
    * def res_get_constant_flag_paris = read('../../../response/backend/get_constant_flag_paris.json')

  @api-test @paris-api-backend @paris-api-backend-negative @paris-backend-login
  Scenario Outline: Paris Backend - Not authorized token
    * set req_token.token = '<token>'
    Given url ambiente.url.backend
    And path ambiente.path.backend.login
    * header Content-Type = content_type.json_app
    * param origin = flags.paris
    And request req_token
    When method post
    Then status 401
    And match response == token_unauthorized

    Examples:
      | token  |
      |        |
      | T0VtABuF1aNFstGni2x+sfqMWs7Sx6kYOZLDZxpG9aYBNL4+c4aiFAUP12oVvfH5la4ohOuhG7oIHN0yUbTR9lEB3GCkYhXfR0L1S6ijNp24LN2rYBirh8wECboF/uF34iwB/9qnZFwH3Blv8GrMYcAWbYr2RzkNdxb2DqxiMbkk1xWj98P2U+ARez+UbKKSN/SRvBsKzYeqyKd/lNv+QSdd3AZhR/mDYkmK8NRrH3s0w+DUYglhV4BWJ84Ee5xN3QhEQJ84v+pbB9uYDAR1Rf6Da8QTN2L3XK/mqBzrs98kQNVhGX1KHvh8SJZCv+nk3+ostdggLFwhE10lCVAoOQ== |

  @api-test @paris-api-backend @paris-api-backend-positive @paris-backend-get-topics
  Scenario: Paris Backend - Get Topics <flag>
    Given url ambiente.url.backend
    * def path = ambiente.path.backend.get_topics
    * def flag_paris = flags.paris
    * replace path
        | token | value      |
        | flag  | flag_paris |
    And path path
    And param origin = flags.paris
    When method get
    Then status 200
    And match response == res_get_topics

  @api-test @paris-api-backend @paris-api-backend-positive @paris-backend-get-bussiness-rules
  Scenario Outline: Paris Backend - Get Bussiness Rules <topic> - <subtopic> - <order>
    * def result = call read('../../../features/paris/api/backend-paris-login.feature')
    * def authToken = 'bearer ' + result.response.accessToken
    Given url ambiente.url.backend
    * def path = ambiente.path.backend.get_bussiness_rules + '<topic>/' + '<subtopic>/' + '<order>'
    And path path
    * header Authorization = authToken
    When method get
    Then status 200
    And match response == '#[<count>] res_get_bussiness_rules'
    And match response[0].numTerminal == '#number'
    And match response[0].numTransaction == '#number'
    And match response[1].numTerminal == '#number'
    And match response[1].numTransaction == '#number'

    Examples:
        | topic | subtopic  | order      | count |
        | 2     | 2         | 2452002331 | 3     |
        | 2     | 2         | 2403895070 | 5     |

  @api-test @paris-api-backend @paris-api-backend-positive @paris-backend-get-constant-flag
  Scenario: Paris Backend - Get Contstant Flag <flag>
    Given url ambiente.url.backend
    * def path = ambiente.path.backend.get_constant_flag
    * def flag_paris = flags.paris
    * replace path
        | token | value      |
        | flag  | flag_paris |
    And path path
    And param origin = flags.paris
    When method get
    Then status 200
    And match response == res_get_constant_flag_paris