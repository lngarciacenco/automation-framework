@Cencosud
Feature: Backend APIs Paris

  Background: Carga de variables
    # Carga de tramas json
    * def req_token = read('../../../request/backend/login_get_token/token.json')
    * def res_token = read('../../../response/backend/login_get_token/token.json')
    * def token_unauthorized = read('../../../response/backend/login_get_token/token_unauthorized.json')
    * def res_get_topics = read('../../../response/backend/get_topics_paris.json')
    * def res_get_bussiness_rules = read('../../../response/backend/get_bussiness_rules.json')

  @api-test @paris-api-backend  @paris-api-backend-positive @paris-backend-login
  Scenario: Paris Backend - Token OK
    * set req_token.token = ambiente.static_data.backend.valid_login_token_paris
    Given url ambiente.url.backend
    And path ambiente.path.backend.login
    * header Content-Type = content_type.json_app
    And param origin = flags.paris
    And request req_token
    When method post
    Then status 201
    And print response
    And match response == res_token
