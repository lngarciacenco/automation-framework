function environmentQA() {

	var ambiente = {
		url: {
			backend: "https://self-service-backend.ecomm-stg.cencosud.com",
		},
		path:{
			backend: {
				login: "/auth/login",
				get_topics: "/content/form/getTopics/<flag>",
				get_bussiness_rules: "/content/form/getBussinessRules/",
				get_constant_flag: "/content/form/getConstantFlag/<flag>"
			}
		},
		static_data: {
			backend: {
				valid_login_token_paris: "T0VtABuF1aNFstGni2x+sfqMWs7Sx6kYOZLDZxpG9aYBNL4+c4aiFAUP12oVvfH5la4ohOuhG7oIHN0yUbTR9lEB3GCkYhXfR0L1S6ijNp24LN2rYBirh8wECboF/uF34iwB/9qnZFwH3Blv8GrMYcAWbYr2RzkNdxb2DqxiMbkk1xWj98P2U+ARez+UbKKSN/SRvBsKzYeqyKd/lNv+QSdd3AZhR/mDYkmK8NRrH3s0w+DUYglhV4BWJ84Ee5xN3QhEQJ84v+pbB9uYDAR1Rf6Da8QTN2L3XK/mqBzrs98kQNVhGX1KHvh8SJZCv+nk3+ostdggLFwhE10lCVAeOQ=="
			}
		}
	};

	return ambiente;
}